//-------------------------------------------------
//
//    Copyright © 2016 Sit Melai
//
//
//    This file is part of Rift-Translation.
//
//    Rift-Translation is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Rift-Translation is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with Rift-Translation.  If not, see <http://www.gnu.org/licenses/>.
//
//-------------------------------------------------

#include "settings.h"
#include "ui_settings.h"

#include <QFileDialog>

Settings::Settings(QList<QVariant> inChannels, QWidget *parent, int inLang, int inOpac, QString inLog) :
    QDialog(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);

    channels = inChannels;
    lang = inLang;
    opac = inOpac;
    log = inLog;

    ui->Around->setChecked(channels[0].toBool());
    ui->Region->setChecked(channels[1].toBool());
    ui->Team->setChecked(channels[2].toBool());
    ui->Universe->setChecked(channels[3].toBool());
    ui->Guild->setChecked(channels[4].toBool());
    ui->User->setChecked(channels[5].toBool());
    ui->Dynamic0->setChecked(channels[6].toBool());
    ui->Dynamic1->setChecked(channels[7].toBool());
    ui->Dynamic2->setChecked(channels[8].toBool());
    ui->Dynamic3->setChecked(channels[9].toBool());
    ui->Dynamic4->setChecked(channels[10].toBool());
    ui->Dynamic5->setChecked(channels[11].toBool());
    ui->langSelector->setCurrentIndex(inLang);
    ui->opacitySlider->setValue(inOpac);
    ui->logLocation->setText(inLog);

    connect(ui->Around, SIGNAL(clicked()), this, SLOT(provUpdateSettings()));
    connect(ui->Region, SIGNAL(clicked()), this, SLOT(provUpdateSettings()));
    connect(ui->Team, SIGNAL(clicked()), this, SLOT(provUpdateSettings()));
    connect(ui->Universe, SIGNAL(clicked()), this, SLOT(provUpdateSettings()));
    connect(ui->Guild, SIGNAL(clicked()), this, SLOT(provUpdateSettings()));
    connect(ui->User, SIGNAL(clicked()), this, SLOT(provUpdateSettings()));
    connect(ui->Dynamic0, SIGNAL(clicked()), this, SLOT(provUpdateSettings()));
    connect(ui->Dynamic1, SIGNAL(clicked()), this, SLOT(provUpdateSettings()));
    connect(ui->Dynamic2, SIGNAL(clicked()), this, SLOT(provUpdateSettings()));
    connect(ui->Dynamic3, SIGNAL(clicked()), this, SLOT(provUpdateSettings()));
    connect(ui->Dynamic4, SIGNAL(clicked()), this, SLOT(provUpdateSettings()));
    connect(ui->Dynamic5, SIGNAL(clicked()), this, SLOT(provUpdateSettings()));
    connect(ui->langSelector, SIGNAL(currentIndexChanged(int)), this, SLOT(provUpdateSettings()));
    connect(ui->opacitySlider, SIGNAL(valueChanged(int)), this, SLOT(provUpdateSettings()));
}

Settings::~Settings()
{
    delete ui;
}

void Settings::provUpdateSettings() {
        channels[0] = ui->Around->isChecked();
        channels[1] = ui->Region->isChecked();
        channels[2] = ui->Team->isChecked();
        channels[3] = ui->Universe->isChecked();
        channels[4] = ui->Guild->isChecked();
        channels[5] = ui->User->isChecked();
        channels[6] = ui->Dynamic0->isChecked();
        channels[7] = ui->Dynamic1->isChecked();
        channels[8] = ui->Dynamic2->isChecked();
        channels[9] = ui->Dynamic3->isChecked();
        channels[10] = ui->Dynamic4->isChecked();
        channels[11] = ui->Dynamic5->isChecked();
        lang = ui->langSelector->currentIndex();
        opac = ui->opacitySlider->value();
}

void Settings::on_logChange_clicked()
{
    log = QFileDialog::getOpenFileName(this,
         tr("Open Chat Log File"), "/home", tr("TXT Files (*.txt)"));
    ui->logLocation->setText(log);
}
