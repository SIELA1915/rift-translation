//-------------------------------------------------
//
//    Copyright © 2016 Sit Melai
//
//
//    This file is part of Rift-Translation.
//
//    Rift-Translation is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Rift-Translation is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with Rift-Translation.  If not, see <http://www.gnu.org/licenses/>.
//
//-------------------------------------------------

#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>
#include "mainwindow.h"

namespace Ui {
class Settings;
}

class Settings : public QDialog
{
    Q_OBJECT

public:
    explicit Settings(QList<QVariant> inChannels, QWidget *parent = 0, int inLang = 0, int inOpac = 100, QString inLog = "");
    ~Settings();
    QList<QVariant> channels;
    int lang;
    QString log;
    int opac;

private slots:
    void provUpdateSettings();

    void on_logChange_clicked();

private:
    Ui::Settings *ui;
};

#endif // SETTINGS_H
