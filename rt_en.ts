<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="35"/>
        <source>Rift-Translation</source>
        <translation>Rift-Translation</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="48"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="66"/>
        <source>About</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="71"/>
        <source>About Qt</source>
        <translation>About Qt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="76"/>
        <source>Settings...</source>
        <translation>Preferences...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="39"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="40"/>
        <source>&amp;About</source>
        <translation>&amp;About</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="42"/>
        <source>About &amp;QT</source>
        <translation>About &amp;QT</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="44"/>
        <source>&amp;Settings</source>
        <translation>&amp;Preferences</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="46"/>
        <source>&amp;Pin Window</source>
        <translation>&amp;Pin Window</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="53"/>
        <source>&amp;Window</source>
        <translation>&amp;Window</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="67"/>
        <source>Around</source>
        <translation>Around</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="67"/>
        <source>Region</source>
        <translation>Region</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="67"/>
        <source>Team</source>
        <translation>Team</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="67"/>
        <source>Universe</source>
        <translation>Universe</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="67"/>
        <source>Guild</source>
        <translation>Guild</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="67"/>
        <source>Dynamic 1</source>
        <translation>Dynamic 1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="67"/>
        <source>Dynamic 2</source>
        <translation>Dynamic 2</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="67"/>
        <source>Dynamic 3</source>
        <translation>Dynamic 3</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="67"/>
        <source>Dynamic 4</source>
        <translation>Dynamic 4</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="67"/>
        <source>Dynamic 5</source>
        <translation>Dynamic 5</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="67"/>
        <source>User</source>
        <translation>User</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="67"/>
        <source>Dynamic 0</source>
        <translation>Dynamic 0</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="125"/>
        <source>About Rift-Translation</source>
        <translation>About Rift-Translation</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="126"/>
        <source>The &lt;b&gt;Rift-Translator&lt;/b&gt; is a tool to translate Chat channels in Ryzom live.Powered by Yandex.Translate:</source>
        <translation>&lt;b&gt;Rift-Translation&lt;/b&gt; is a tool to translate Chat channels in Ryzom live.Powered by Yandex.Translate:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="185"/>
        <source>Translating to </source>
        <translation>Translating to </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="185"/>
        <source> Powered by Yandex.Translate: </source>
        <translation> Powered by Yandex.Translate: </translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="settings.ui" line="17"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="settings.ui" line="50"/>
        <source>Select language 
to translate to:</source>
        <translation>Select language 
to translate to:</translation>
    </message>
    <message>
        <location filename="settings.ui" line="62"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="settings.ui" line="67"/>
        <source>German</source>
        <translation>German</translation>
    </message>
    <message>
        <location filename="settings.ui" line="72"/>
        <source>French</source>
        <translation>French</translation>
    </message>
    <message>
        <location filename="settings.ui" line="77"/>
        <source>Spanish</source>
        <translation>Spanish</translation>
    </message>
    <message>
        <location filename="settings.ui" line="82"/>
        <source>Russian</source>
        <translation>Russian</translation>
    </message>
    <message>
        <location filename="settings.ui" line="112"/>
        <source>Change log location</source>
        <translation>Change log location</translation>
    </message>
    <message>
        <location filename="settings.ui" line="123"/>
        <source>Around</source>
        <translation>Around</translation>
    </message>
    <message>
        <location filename="settings.ui" line="130"/>
        <source>Dynamic 0</source>
        <translation>Dynamic 0</translation>
    </message>
    <message>
        <location filename="settings.ui" line="144"/>
        <source>Dynamic 1</source>
        <translation>Dynamic 1</translation>
    </message>
    <message>
        <location filename="settings.ui" line="137"/>
        <source>Region</source>
        <translation>Region</translation>
    </message>
    <message>
        <location filename="settings.ui" line="158"/>
        <source>Dynamic 2</source>
        <translation>Dynamic 2</translation>
    </message>
    <message>
        <location filename="settings.ui" line="151"/>
        <source>Team</source>
        <translation>Team</translation>
    </message>
    <message>
        <location filename="settings.ui" line="172"/>
        <source>Dynamic 3</source>
        <translation>Dynamic 3</translation>
    </message>
    <message>
        <location filename="settings.ui" line="165"/>
        <source>Universe</source>
        <translation>Universe</translation>
    </message>
    <message>
        <location filename="settings.ui" line="186"/>
        <source>Dynamic 4</source>
        <translation>Dynamic 4</translation>
    </message>
    <message>
        <location filename="settings.ui" line="179"/>
        <source>Guild</source>
        <translation>Guild</translation>
    </message>
    <message>
        <location filename="settings.ui" line="193"/>
        <source>User</source>
        <translation>User</translation>
    </message>
    <message>
        <location filename="settings.ui" line="200"/>
        <source>Dynamic 5</source>
        <translation>Dynamic 5</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="96"/>
        <source>Open Chat Log File</source>
        <translation>Open Chat Log File</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="96"/>
        <source>TXT Files (*.txt)</source>
        <translation>TXT Files (*.txt)</translation>
    </message>
</context>
</TS>
