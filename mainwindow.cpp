//-------------------------------------------------
//
//    Copyright © 2016 Sit Melai
//
//
//    This file is part of Rift-Translation.
//
//    Rift-Translation is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Rift-Translation is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with Rift-Translation.  If not, see <http://www.gnu.org/licenses/>.
//
//-------------------------------------------------

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QPushButton>
#include <QSignalMapper>
#include <QtNumeric>
#include <QScrollBar>
#include <QtNetwork>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    readSettings();

    QMenu *helpMenu = ui->menubar->addMenu(tr("&Help"));
    QAction *aboutAction = new QAction(tr("&About"), this);
    connect(aboutAction, SIGNAL(triggered(bool)), this, SLOT(about()));
    QAction *aboutQtAction = new QAction(tr("About &QT"), this);
    connect(aboutQtAction, SIGNAL(triggered(bool)), qApp, SLOT(aboutQt()));
    QAction *channelSettings = new QAction(tr("&Settings"), this);
    connect(channelSettings, SIGNAL(triggered(bool)), this, SLOT(openSettings()));
    QAction *pinWindow = new QAction(tr("&Pin Window"), this);
    pinWindow->setCheckable(true);
    connect(pinWindow, SIGNAL(triggered(bool)), this, SLOT(pinWindow(bool)));
    helpMenu->addAction(aboutAction);
    helpMenu->addAction(aboutQtAction);
    helpMenu->addSeparator();
    helpMenu->addAction(channelSettings);
    QMenu *windowMenu = ui->menubar->addMenu(tr("&Window"));
    windowMenu->addAction(pinWindow);

    QSettings settings("SIELA1915", "Rift-Translation");

    logLoc = settings.value("log", "").toString();

    destLang = settings.value("lang", 0).toInt();

    QList<QVariant> chanDef({1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0});

    selChannels = settings.value("channels", QVariant::fromValue(chanDef)).toList();
    if (selChannels.size() != MAX_CHAN) selChannels = chanDef;
    oldChannels = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    channelNames = {tr("Around"), tr("Region"), tr("Team"), tr("Universe"), tr("Guild"), tr("User"), tr("Dynamic 0"), tr("Dynamic 1"), tr("Dynamic 2"), tr("Dynamic 3"), tr("Dynamic 4"), tr("Dynamic 5")};
    Q_ASSERT(channelNames.size() == MAX_CHAN);

    //    QSignalMapper *mapper = new QSignalMapper(this);

    for (int i = 0; i < channelNames.size(); ++i) {
        tabs.append(new QPushButton(channelNames[i], this));
        tabs[i]->setVisible(false);
        tabs[i]->setCheckable(true);
        //        mapper->setMapping(tabs[i], i);
        //        connect(tabs[i], SIGNAL(clicked(bool)), mapper, SLOT(map()));
    }

    actTabs = new QButtonGroup();

    //    connect(mapper, SIGNAL(mapped(int)), this, SLOT(updateText(int)));
    connect(actTabs, SIGNAL(buttonToggled(int,bool)), this, SLOT(updateText(int,bool)));
    updateTabs();
    chat_update.start(1000, this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    writeSettings();
    event->accept();
}

void MainWindow::writeSettings()
{
    QSettings settings("SIELA1915", "Rift-Translation");

    settings.beginGroup("MainWindow");
    settings.setValue("size", size());
    settings.setValue("pos", pos());
    settings.setValue("opac", (double)opacity/100);
    settings.endGroup();
}

void MainWindow::readSettings()
{
    QSettings settings("SIELA1915", "Rift-Translation");

    settings.beginGroup("MainWindow");
    resize(settings.value("size", QSize(400, 400)).toSize());
    move(settings.value("pos", QPoint(200, 200)).toPoint());
    setWindowOpacity((double)settings.value("opac", 1).toDouble());
    opacity = (int)(settings.value("opac", 1).toDouble()*100);
    qInfo("Opacity is: %f", settings.value("opac", 1).toDouble());
    settings.endGroup();
}

void MainWindow::about()
{
    QMessageBox::about(this, tr("About Rift-Translation"),
                       tr("The <b>Rift-Translator</b> is a tool to translate "
                          "Chat channels in Ryzom live."
                          "Powered by Yandex.Translate:") +
                          " <a style = 'color:rgb(0,0,0); text-decoration:none' href=\"http://translate.yandex.com/\">translate.yandex.com</a>");
}

void MainWindow::pinWindow(bool p)
{
    Qt::WindowFlags flags = this->windowFlags();
    if (p)
    {
        this->setWindowFlags(flags | Qt::CustomizeWindowHint | Qt::WindowStaysOnTopHint);
        this->show();
    }
    else
    {
        this->setWindowFlags(flags ^ (Qt::CustomizeWindowHint | Qt::WindowStaysOnTopHint));
        this->show();
    }
}

void MainWindow::openSettings()
{
    qInfo("Opacity before: %i", opacity);
    Settings *setting = new Settings(selChannels, this, destLang, opacity, logLoc);
    if (setting->exec()) {
        oldChannels = selChannels;
        selChannels = setting->channels;
        destLang = setting->lang;
        opacity = setting->opac;
        qInfo("Opacity after: %i", opacity);
        setWindowOpacity((double)(opacity)/100);
        logLoc = setting->log;
        QSettings settings("SIELA1915", "Rift-Translation");
        settings.setValue("channels", selChannels);
        settings.setValue("lang", destLang);
        settings.setValue("log", logLoc);
        settings.setValue("MainWindow/opac", opacity/100);
        updateTabs();
    }
}

void MainWindow::updateTabs() {
    auto layout = dynamic_cast<QVBoxLayout*>(ui->centralwidget->layout());
    auto hLayout = dynamic_cast<QHBoxLayout*>(layout->itemAt(0));
    for (int i = 0; i < tabs.size(); ++i) {
        qInfo("treating pushButton: %i", i);
        if (selChannels[i].toBool() && !oldChannels[i].toBool()) {
            tabs[i]->setVisible(true);
            hLayout->addWidget(tabs[i]);
            actTabs->addButton(tabs[i], i);
            qInfo("activating with name: %s", channelNames[i].toStdString().c_str());
        } else if (!selChannels[i].toBool() && oldChannels[i].toBool()) {
            tabs[i]->setVisible(false);
            actTabs->removeButton(tabs[i]);
            hLayout->removeWidget(tabs[i]);
            qInfo("deactivating with name: %s", channelNames[i].toStdString().c_str());
        }
    }
    ui->label->setText(tr("Translating to ") + tr(languages[destLang].toStdString().c_str()) + tr(" Powered by Yandex.Translate: ") + " <a style = 'color:rgb(0,0,0); text-decoration:none' href=\"http://translate.yandex.com/\">translate.yandex.com</a>");
    int i = 0;
    while (selChannels[i] == 0) {
        ++i;
    }
    if (i < 10) tabs[i]->click();
}

QString MainWindow::translateTo(QString *lang, QString *text) {

    // create custom temporary event loop on stack
    QEventLoop eventLoop;

    // "quit()" the event-loop, when the network request "finished()"
    QNetworkAccessManager mgr;
    QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

    QString apiKey = "trnsl.1.1.20160523T152228Z.1fc1df4272b9a9d3.0a832d942658058f208980f33bd6c14cb9db2336";

    // the HTTP request
    QNetworkRequest req( QUrl( QString("https://translate.yandex.net/api/v1.5/tr.json/translate?key=" + apiKey + "&text=" + *text + "&lang=" + *lang) ) );
    QNetworkReply *reply = mgr.get(req);
    eventLoop.exec(); // blocks stack until "finished()" has been called

    if (reply->error() == QNetworkReply::NoError) {

        QString strReply = (QString)reply->readAll();

        //parse json
        qDebug() << "Response:" << strReply;
        QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());

        QJsonObject jsonObj = jsonResponse.object();

        qDebug() << "translating: " << jsonObj["lang"].toString();
        QString res = jsonObj["text"].toArray().at(0).toString();
        qDebug() << "text: " << res;

        return res;
    }
    else {
        //failure
        qDebug() << "Failure" << reply->errorString();
        return "Couldn't reach Yandex.Translate because of " + reply->errorString() + "\n";
   }
}

void MainWindow::fileLinesFromEnd(QFile *chatLog, int lines) {
    chatLog->seek(chatLog->size()-1);
    int count = 0;
    while ((count < lines+1) && (chatLog->pos() > 0)) {
        QString ch = chatLog->read(1);
        chatLog->seek(chatLog->pos()-2);
        if (ch == "\n") count++;
    }
    chatLog->seek(chatLog->pos()+2);
}

void MainWindow::updateText(int b, bool c, int lines) {
    if (!c) return;
    qInfo("checked button: %i", b);
    if (curChannel != b) ui->chat_text->setText("");
    curChannel = b;

    QScrollBar *sb = ui->chat_text->verticalScrollBar();
    bool low = sb->value() == sb->maximum();
    //    low = true;
    QFile chatLog(logLoc);
    QString filteredLog = "";
    if (chatLog.open(QIODevice::ReadOnly)) {
        fileLinesFromEnd(&chatLog, lines);
        QString tempLastLine = "";
        bool append = false;
        while (!chatLog.atEnd()) {
            char buf[1024];
            qint64 lineLength = chatLog.readLine(buf, sizeof(buf));
            QString line(buf);
            tempLastLine = line;
            if (lineLength != -1 && (append || (lines == 100))) {
                QString sec = line.section(' ', 2, 2);
                if (sec == "(" + channelCodes[b] + ")" || channelCodes[b] == "") {
                    //                qInfo("found line: %s", line.toStdString().c_str());
                    QString translated = translateTo(&languageCodes[destLang], &line);
                    filteredLog += translated;
                }
            }
            if (line == lastLine) append = true;
        }
        lastLine = tempLastLine;
    }
    filteredLog = "\n" + filteredLog.left(filteredLog.size()-1);
    if (filteredLog.size() > 10) {
        QCursor c = ui->chat_text->cursor();
        ui->chat_text->moveCursor(QTextCursor::End);
        ui->chat_text->insertPlainText(filteredLog);
        ui->chat_text->setCursor(c);
    }
    if (low) sb->setValue(sb->maximum());
}
