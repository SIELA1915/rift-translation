#-------------------------------------------------
#
#    Copyright © 2016 Sit Melai
#
#
#    This file is part of Rift-Translation.
#
#    Rift-Translation is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Rift-Translation is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with Rift-Translation.  If not, see <http://www.gnu.org/licenses/>.
#
#-------------------------------------------------

QT       += core gui network
CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Rift-Translation
TEMPLATE = app


SOURCES += main.cpp\
    settings.cpp \
    mainwindow.cpp

HEADERS  += \
    settings.h \
    mainwindow.h

FORMS    += \
    settings.ui \
    mainwindow.ui

TRANSLATIONS = rt_en.ts \
    rt_de.ts
