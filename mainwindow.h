//-------------------------------------------------
//
//    Copyright © 2016 Sit Melai
//
//
//    This file is part of Rift-Translation.
//
//    Rift-Translation is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Rift-Translation is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with Rift-Translation.  If not, see <http://www.gnu.org/licenses/>.
//
//-------------------------------------------------

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>
#include <QSettings>
#include <QMessageBox>
#include <QButtonGroup>
#include <QTimer>

#include "settings.h"

#define MAX_CHAN 12

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    void timerEvent(QTimerEvent * ev) {
      if (ev->timerId() != chat_update.timerId()) {
        QWidget::timerEvent(ev);
        return;
      }
      updateText(curChannel, true, 5);
    }

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void closeEvent(QCloseEvent *event);

private slots:
    void about();
    void openSettings();
    void updateText(int b, bool c, int buf = 100);
    void pinWindow(bool p = true);

private:
    Ui::MainWindow *ui;
    QList<QVariant> selChannels;
    QList<QVariant> oldChannels;
    QList<QPushButton*> tabs;
    QButtonGroup *actTabs;
    QList<QString> channelNames;
    QString channelCodes[MAX_CHAN] = {"SAY", "REGION", "TEAM", "UNIVERSE", "GUILD", "", "DYN0", "DYN1", "DYN2", "DYN3", "DYN4", "DYN5"};
    QString languages[5] = {"English", "German", "French", "Spanish", "Russian"};
    QString languageCodes[5] = {"en", "de", "fr", "es", "ru"};
    int destLang;
    int opacity;
    int curChannel;
    QString logLoc;
    QString lastLine;
    QBasicTimer chat_update;
    QString translateTo(QString *lang, QString *text);
    void fileLinesFromEnd(QFile *chatLog, int lines = 1);
    void readSettings();
    void writeSettings();
    void updateTabs();
};

#endif // MAINWINDOW_H
